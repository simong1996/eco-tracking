import { useContext } from 'react'

import { AuthContext, AuthContextType } from '../contexts/Auth'

export const useAuth = (): AuthContextType => {
  return useContext(AuthContext)
}
