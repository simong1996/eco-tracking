import { Button, TextField, Typography } from '@material-ui/core'
import { useForm } from 'react-hook-form'
import { Link, useNavigate } from 'react-router-dom'

import { useAuth } from '../../../hooks/useAuth'

const Register = (): JSX.Element => {
  const {
    handleSubmit,
    register,
    setError,
    formState: { errors },
  } = useForm()
  const navigate = useNavigate()
  const { register: authRegister } = useAuth()

  const onSubmit = async (values: { email: string; password: string }) => {
    const errors = await authRegister({
      email: values.email,
      password: values.password,
    })

    if (errors) {
      errors.forEach(error => {
        if (error) setError(error.name, { message: error.message })
      })
    } else {
      navigate('/')
    }
  }

  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <div style={{ width: '400px' }}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <TextField
            name="email"
            placeholder="Email"
            inputRef={register}
            fullWidth
            style={{ marginBottom: '20px' }}
          />
          {errors.email && <p>{errors.email.message}</p>}
          <TextField
            type="password"
            name="password"
            placeholder="Password"
            inputRef={register}
            style={{ marginBottom: '20px' }}
            fullWidth
          />
          {errors.password && <p>{errors.password.message}</p>}
          <Button variant="outlined" color="primary" type="submit">
            Register
          </Button>
          <Button
            style={{ marginLeft: '20px' }}
            color="secondary"
            type="button">
            <Link style={{ textDecoration: 'none' }} to="/login">
              <Typography color="textPrimary">Login</Typography>
            </Link>
          </Button>
        </form>
      </div>
    </div>
  )
}

export default Register
